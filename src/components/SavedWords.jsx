import WordContext from "../context/WordContext"
import {useContext} from 'react'
import {BsFillCheckCircleFill} from 'react-icons/bs'

function SavedWords() {

    const {savedWords} = useContext(WordContext)

    return (
        <div className="flow-root">
            <div className="badge badge-primary badge-lg py-3 opacity-40 text-sm hover:opacity-100"><BsFillCheckCircleFill className="pr-3 text-3xl"/>Words Saved: {savedWords.length} </div>
        </div>
    )
}

export default SavedWords