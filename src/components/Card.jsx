import WordContext from '../context/WordContext'
import {useContext, useState} from 'react'
import {BsFillBookmarkHeartFill} from 'react-icons/bs'
import SavedWords from './SavedWords'

function Card() {

    const{words, index, isSaved, newIndex, addSavedWord} = useContext(WordContext)

    return (
       <div>
        <div className="container mx-auto">
            <div className="p-6 grid grid-cols-1 max-w-sm mx-auto bg-white rounded-md shadow-lg content-center">
                <div className="grid grid-cols-2 pb-3">
                    <SavedWords/>
                    <div className="flow-root">
                        {(isSaved[words[index].id] === true) && <BsFillBookmarkHeartFill className="text-4xl fill-primary float-right"/>}
                        {(isSaved[words[index].id] === false) && <BsFillBookmarkHeartFill className="text-4xl fill-transparent float-right"/>}
                    </div>
                </div>
                <div className="text-center text-black text-lg font-bold py-3 pt-1">{words[index].name}</div>
                <div className="text-center text-black italic py-3">{words[index].desc}</div>
                <div className="text-center text-black font-serif py-3">"{words[index].example}"</div>
                <div className="btn-group grid grid-cols-2 content-center">
                    <button onClick={addSavedWord} className="btn btn-primary btn-outline btn-md mt-3">Save Word</button>
                    <button onClick={newIndex} className="btn btn-secondary btn-outline btn-md mt-3">Next</button>
                </div>
            </div>
        </div>
       </div> 
    )

}

export default Card