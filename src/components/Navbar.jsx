import { GiAnvil } from 'react-icons/gi'
import { Link } from 'react-router-dom'

function Navbar() {

    return (
        <nav className="navbar mb-12 shadow-lg bg-neutral-content text-base-300">
                <div className="container mx-auto">
                    <div className="flex-none px-0 mx-0">
                    <GiAnvil className="inline pr-1 text-4xl"/>
                        <span className="text-md font-bold align-middle">Wordsmith</span>
                    </div>
                    <div className="flex-1 px-0 mx-0">
                        <div className="flex justify-end">
                            <Link to='/' className="btn btn-ghost btn-sm rounded-btn">
                                Home
                            </Link>
                            <Link to='/saved'  className="btn btn-ghost btn-sm rounded-btn">
                                View Saved
                            </Link>
                            <Link to='/about' className="btn btn-ghost btn-sm rounded-btn">
                                About
                            </Link>
                        </div>
                    </div>
                </div>
            </nav>

    )
}

export default Navbar