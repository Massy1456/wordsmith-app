import { createContext, useState, useEffect } from "react"
import {wordsData} from '../data/WordData'

const WordContext = createContext()

export const WordProvider = ({children}) => {


    const [isSaved, setIsSaved] = useState([]) // array of booleans to see if a particular word is saved
    const [words, setWords] = useState(wordsData) // holds word data
    const [savedWords, setSavedWords] = useState([]) // stores the actual saved word
    const [index, setIndex] = useState(0) // index number

    useEffect(() => {
        for(let i = 0; i < words.length; i++){
            isSaved[i] = false;
        }
    }, [])

    const newIndex = () => {
        let n = Math.floor(Math.random() * words.length)
        console.log(words.length)
        setIndex(n)
    }

    // saves the word if it isnt saved yet and will unsave it if it's already saved
    const addSavedWord = () => {
        if(isSaved[words[index].id] === false){
            isSaved[words[index].id] = true
            setSavedWords([words[index], ...savedWords])
        } else if (isSaved[words[index].id] === true){
            isSaved[words[index].id] = false
            setSavedWords(savedWords.filter((item) => item.id !== words[index].id))
        }
    }

    return (
        <WordContext.Provider value={{words: words, index: index, savedWords: savedWords, isSaved: isSaved, newIndex, addSavedWord}}>
            {children}
        </WordContext.Provider>
    )

}

export default WordContext