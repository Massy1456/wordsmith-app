import {useContext, useState} from 'react'
import WordContext from '../context/WordContext'
import {FaBookOpen} from 'react-icons/fa'

function Saved() {

    const {savedWords} = useContext(WordContext)


    if(savedWords.length === 0){
        return (
            <div className="flex font-bold text-3xl text-center items-center justify-center text-gray-200">You don't have any words saved yet.</div>
        )
    } else {
        return (
        <div>
        <div className="text-center font-bold text-3xl text-gray-200 mb-5">Saved Words</div>
        <div className="flex items-center justify-center">
            <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 p-5">
                {savedWords.map((item) => (
                        <div className="bg-white text-black text-xl font-bold text-center p-10 pb-5 rounded-lg hover:bg-slate-100" key={item.id}>{item.name}
                            <div className="grid grid-col-1 pt-5">
                            <label htmlFor={item.id} class="btn btn-md btn-secondary btn-outline modal-button">View</label> 
                            <input type="checkbox" id={item.id} className="modal-toggle"/> 
                            <div className="modal ">
                            <div className="modal-box bg-white shadow-xl ">
                                <p className="text-center text-black text-xl underline underline-offset-2 font-bold py-3 pt-1" key={item.id}>{item.name}</p>
                                <p className="text-center text-black font-normal italic py-3 text-md" key={item.id}>{item.desc}</p>
                                <p className="text-center text-black font-normal font-serif py-3 text-md" key={item.id}>"{item.example}"</p>
                                <div className="modal-action">
                                <label htmlFor={item.id} className="btn btn-secondary btn-sm btn-outline">Close</label>
                                <label htmlFor={item.id} className="btn btn-secondary btn-sm btn-outline"><FaBookOpen className="text-3xl pr-3"/>Learned</label>
                                </div>
                            </div>
                            </div>
                            </div>
                        </div>
                ))}
            </div>
        </div>
        </div>
        )

    }


}

export default Saved