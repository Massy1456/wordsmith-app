import Card from "./components/Card"
import Saved from "./pages/Saved"
import Navbar from "./components/Navbar"
import About from "./pages/About"
import {WordProvider} from './context/WordContext'
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'

function App() {
  return (
    <Router>
    <WordProvider>
        <div className="App">
          <Navbar/>
          <Routes>
            <Route exact path='/' element={<Card />} />
            <Route path='/saved' element={<Saved />} />
            <Route path='/about' element={<About />} />
          </Routes>
        </div> 
    </WordProvider>
    </Router>
  );
}

export default App;
